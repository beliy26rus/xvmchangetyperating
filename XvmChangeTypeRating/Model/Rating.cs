﻿using System;

namespace XvmChangeTypeRating.Model
{
    [Serializable]
    public class Rating
    {
        public string Designation { get; set; }
        public string Description { get; set; }
    }
}
