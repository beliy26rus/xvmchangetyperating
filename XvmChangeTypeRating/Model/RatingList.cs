﻿using System;
using System.Collections.Generic;

namespace XvmChangeTypeRating.Model
{
    [Serializable]
    public class RatingList
    {
        public List<Rating> Ratings { get; set; }
    }
}
