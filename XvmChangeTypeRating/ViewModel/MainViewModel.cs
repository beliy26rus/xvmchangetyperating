using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Xml.Serialization;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using XvmChangeTypeRating.Model;

namespace XvmChangeTypeRating.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {

        private const string ScriptgFile = @"\Script\any_to_eff.js";
        private readonly string _scriptFilePath;
        private const string RatingConfigFile = @"\Configs\Ratings";
        private readonly string _ratingConfigFilePath;
      

        private List<Rating> _ratings;
        public List<Rating> Ratings
        {
            get { return _ratings; }
            set
            {
                _ratings = value;
                RaisePropertyChanged(() => Ratings);
            }
        }


        private Rating _curretRating;
        public Rating CurrentRating
        {
            get { return _curretRating; }
            set
            {
                _curretRating = value;
                RaisePropertyChanged(() => CurrentRating);
            }
        }

        private string _directoryConfig;
        public string DirectoryConfig
        {
            get { return _directoryConfig; }
            set
            {
                _directoryConfig = value;
                RaisePropertyChanged(() => DirectoryConfig);
            }
        }


        public RelayCommand ShowFolderDialogRelayCommand { get; private set; }
        public RelayCommand ReplaceRatingRelayCommand { get; private set; }

        private void GetFolderPath()
        {
            using (var dialog = new FolderBrowserDialog())
            {
                if (dialog.ShowDialog() != DialogResult.OK) return;
                DirectoryConfig = dialog.SelectedPath;
                Properties.Settings.Default.FolderConfig = DirectoryConfig;
                Properties.Settings.Default.Save();
            }
        }
        private List<Rating> GetRatingsFromXml()
        {
            if (!File.Exists(_ratingConfigFilePath)) return null;
            var ratingList = new RatingList();
            var serializer = new XmlSerializer(ratingList.GetType());
            TextReader reader = new StreamReader(_ratingConfigFilePath);
            ratingList = serializer.Deserialize(reader) as RatingList;
            return ratingList == null ? null : ratingList.Ratings;

        }

        private void ReplaceRating()
        {
            if (!File.Exists(_scriptFilePath) || !Directory.Exists(DirectoryConfig) || CurrentRating == null) return;
            var startInfo = new ProcessStartInfo
            {
                FileName = "cscript.exe",
                Arguments = string.Concat(_scriptFilePath, " ", CurrentRating.Designation," ","\"", DirectoryConfig,"\""),
                WindowStyle = ProcessWindowStyle.Hidden
            };
            Process.Start(startInfo);
            MessageBox.Show(Properties.Resources.SuccesMessage);

        }


        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            var executePath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            _ratingConfigFilePath = string.Concat(executePath, RatingConfigFile,".",CultureInfo.CurrentCulture.ToString(),".xml");
            if (!File.Exists(_ratingConfigFilePath))
            {
                _ratingConfigFilePath = string.Concat(executePath, RatingConfigFile, ".xml");
            }
            _scriptFilePath = string.Concat(executePath, ScriptgFile);
            Ratings = GetRatingsFromXml();
            DirectoryConfig = Properties.Settings.Default.FolderConfig;
            ShowFolderDialogRelayCommand = new RelayCommand(GetFolderPath);
            ReplaceRatingRelayCommand = new RelayCommand(ReplaceRating, () => !string.IsNullOrEmpty(DirectoryConfig) && CurrentRating != null);



        }
    }
}